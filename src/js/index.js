import '../scss/style.scss'; //NIE USUWAJ
import { width } from 'window-size';

//1.SPRAWDZENIE MAKSYMALNEJ LICZBY:
const a = 2;
const b = 3;
const c = 4;
const max = Math.max(a,b,c)
switch (max) {
    case 2:
//        console.log('a jest największa');
        break;
    case 3:
//        console.log('b jest największa');
        break;
    default:
//        console.log('c jest największa');
}

//2.WERYFIKACJA WIEKU, FORMULARZ:
document.querySelector('#myForm').addEventListener('submit', function(e) {
    e.preventDefault();
    let age = document.querySelector('#formAge').value;
    age = parseInt(age, 10);
    age >= 18 ? console.log('wszystko ok'): console.log('dostęp zablokowany');
});

//3.STRING:

let array = [];
let string = "";
for (var i = 0; i <= 9; i++) {
    array[i] = i + 1;
}
for (var i = 0; i < array.length; i++) {
    string = string + " " + array[i];
}

let str = "";
for (var i=1; i<=10; i++) {
    str += i;
}

let text = 'Ala ma kota:'
const number = text.length;
function textAll() {
    text = text + ' ' + number;
}
textAll();

function printText(txt) {
    return "Liczba liter: " + txt.length;
}
//console.log(printText("Ala ma kota"));

//4.Write three functions that compute the sum of the numbers in a given list using a for-loop, a while-loop, and recursion.
const digit = Math.floor(Math.random()*100)+1;
const Array = [];
let sum = 0;
for (var i = 0; i < 10; i++) {
    let digit = Math.floor(Math.random()*100)+1;
    Array.push(digit);
}
for (let i = 0; i < Array.length; i++) {
    sum += Array[i];
}
let x = 0;
while (x < Array.length) {
    sum += Array[x];
    x++;
}

let action = 0;
function addition(n = 0) {
    if ((n + 1) <= Array.length) {
        action += Array[n];
        addition(n + 1);
    } else {
        return;
    }
}
addition();

//5.Write a function that combines two lists by alternatingly taking elements. For example: given the two lists [a, b, c] and [1, 2, 3], the function should return [a, 1, b, 2, c, 3].
const Big = ['A','B','C','D','E','F','G'];
const Small = ['a','b','c','d'];
const Summary = [];
let longer = 0;
let shorter = 0;
let BigIsLonger = false;
let SmallIsLonger = false;

if (Big.length >= Small.length) {
    longer = Big.length;
    shorter = Small.length
    BigIsLonger = true;
} else {
    longer = Small.length;
    shorter = Big.length
    SmallIsLonger = true;
}

for (var i = 0; i < shorter; i++) {
    Summary.push(Big[i]);
    Summary.push(Small[i]);
}
for (var i = shorter; i < longer; i++) {
    if (BigIsLonger) {
        Summary.push(Big[i]);
    } else {
        Summary.push(Small[i]);
    }
}

//6.Write a function that computes the list of the first 100 Fibonacci numbers. By definition, the first two numbers in the Fibonacci sequence are 0 and 1, and each subsequent number is the sum of the previous two. As an example, here are the first 10 Fibonnaci numbers: 0, 1, 1, 2, 3, 5, 8, 13, 21, and 34.
//6a.rekurencja
const series = [];
let suma = 0;
function Fibonacci(n = 0) {
    if (n === 0 || n === 1) {
        series.push(n);
    } if (n > 1) {
        suma = series[n-1] + series[n-2];
        series.push(suma);
    } if (series.length === 100) {
        return
    }
    Fibonacci(n + 1)
}
Fibonacci();

//6b.pętla for
const FibArray = [];
let FibSum = 0;
for (let i = 0; i < 100; i++) {
    if (i === 0 || i === 1) {
        FibArray.push(i);
    } else {
        FibSum = FibArray[i-1] + FibArray[i-2];
        FibArray.push(FibSum);
    }
}

//7.Write a function that given a list of non negative integers, arranges them such that they form the largest possible number. For example, given [50, 2, 1, 9], the largest formed number is 95021.
const ArrInt = [7, 540, 54, 55, 550, 556];
let ArrIntSort = [];
let NumbString = "";   //NumbString = ArrIntSort.join("")
let firstNumb = "";
let secondNumb = "";

/*for (var i = 0; i < 5; i++) {
    let NumberInt = Math.floor(Math.random()*50)
    ArrInt.push(NumberInt);
}
console.log(ArrInt)*/

ArrIntSort = ArrInt.sort()
ArrIntSort.reverse()

function check(firstNumb, secondNumb) {
//    console.log("Będziemy srawdzać czy to nie ten przypadek", firstNumb, secondNumb)
    for (var j = 0; j < firstNumb.length; j++) {
//        console.log('petla', j, firstNumb.length)
        /*if (secondNumb.charAt(j) !== "" && parseInt(firstNumb.charAt(j),10) > parseInt(secondNumb.charAt(j))) {
            continue;
        }*/

        if (secondNumb.charAt(j) === "") {
//            console.log("Brak ostatniej cyfry")
            if (parseInt(firstNumb.charAt(j),10) >= parseInt(secondNumb.charAt(0))) {
                return;
            } else {
//                console.log("mamy trudny przypadek")
            /*    let firstNumbNew = ArrIntSort[i+1];
                let secondNumbNew = ArrIntSort[i];
                ArrIntSort.splice(indexOf(firstNumb, 1, firstNumbNew))
                ArrIntSort.splice(indexOf(secondNumb, 1, secondNumbNew)) */
            }
        } else {
            return;
        }
    }
}
for (var i = 0; (i + 1) < ArrIntSort.length; i++) {
    firstNumb = ArrIntSort[i].toString();
//    console.log(firstNumb);
    secondNumb = ArrIntSort[i+1].toString();
//    console.log(secondNumb);
    if (firstNumb.length >= secondNumb.length) {
//        console.log("firstNumb.length >= secondNumb.length")
        check(firstNumb, secondNumb);
    }
}
//console.log(ArrIntSort)

//8.Write a program that outputs all possibilities to put + or - or nothing between the numbers 1, 2, ..., 9 (in this order) such that the result is always 100. For example: 1 + 2 + 34 – 5 + 67 – 8 + 9 = 100.
const ArrGrow = [1, 2, 3, 4, 5, 6, 7, 8, 9];

//9. Napisz funkcję, która z podanej tablicy wypisze dane typu number.
const Array1 = [2, 3, 'r', 8, 'df', 10, 5, 'k', '@', '7'];
let Array2 =[];

for (var i = 0; i < Array1.length; i++) {
    if (typeof Array1[i] === 'number') {
        Array2.push(Array1[i])
    }
}
//console.log(Array2)

//10.
let digitA = Math.floor(Math.random()*100);
//console.log(digitA);
if (digitA >= 0 && digitA <= 10) {
//    console.log('true')
} else {
//    console.log('fals')
}

//11. Napisać skrypt pytający o 11 liczb i wyświetlający ich sumę oraz średnią.
let Eleven = [];
for (var i = 0; i < 11; i++) {
    let OneDigit = Math.floor(Math.random()*100);
    Eleven.push(OneDigit)
}
function oblicz() {
    let sum = 0;
    for (var i = 0; i < Eleven.length; i++) {
        sum += Eleven[i]
    }
//    console.log(sum)
//    console.log(sum / Eleven.length)
}
oblicz();

//12. Użytkownik podaje liczby a,b,c. Napisz skrypt wyświetlający je w kolejności od najmniejszej do największej.
const ThreeNumbers = [];
document.querySelector('#order').addEventListener('click', function(e) {
    e.preventDefault();
    const one = document.querySelector('#one').value;
    const two = document.querySelector('#two').value;
    const three = document.querySelector('#three').value;
/*    one = parseInt(one, 10);
    two = parseInt(two, 10);
    three = parseInt(three, 10);
    ThreeNumbers.push(one, two, three).sort((a,b) => a-b);
    console.log(ThreeNumbers)
    ThreeNumbers = []; */
    console.log([Number(one), Number(two), Number(three)].sort((a,b) => a-b))
    document.querySelector('#one').value = '';
    document.querySelector('#two').value = '';
    document.querySelector('#three').value = '';
})

//13. Napisz aplikację, która po wprowadzeniu daty urodzin obliczy długość życia w dniach.
const count = () => {
    const dateNow = Date.now();
    const year = document.getElementById('year').value;
    const month = document.getElementById('month').value;
    const day = document.getElementById('day').value;
    const parsedDate = Date.parse(new Date(year + '.' + month + '.' + day));
    const daysFromBirthday = Math.floor((dateNow - parsedDate)/(1000 * 60 * 60 * 24));
    document.querySelector('.result').textContent = daysFromBirthday;
}
document.getElementById('count').addEventListener('click', count)

//14. Użytkownik podaje dwie liczby a i b. Napisz skrypt obliczający wyrażenie: √a/√b. Uwzględnij sprawdzenie czy wyrażenie pod pierwiastkiem nie będzie ujemne.
document.getElementById('calculateTheExpression').addEventListener('click', function(e) {
    e.preventDefault();
    const a = parseInt(document.getElementById('first').value, 10);
    const b = parseInt(document.getElementById('second').value, 10);
    a > 0 && b > 0 ? console.log(Math.sqrt(a) / Math.sqrt(b)): console.log('podaj dwie liczby dodatnie')
})

//15. Zapytaj użytkownika o liczbę n i tekst. Wyświetl na stronie szlaczek o długości n, złożony ze znaku '*' oraz tekstu podanego przez użytkownika.
document.getElementById('trail').addEventListener('click', function(e) {
    e.preventDefault();
    const numberOfCharacters = parseInt(document.getElementById('length').value, 10);
    const tekst = document.getElementById('tekst').value;
    if (numberOfCharacters < (tekst.length + 2)) {
        console.log('podaj większą liczbę znaków');
        return;
    }
    let frontStar = "";
    let backStar = "";
    for (let i = 0; i < Math.ceil((numberOfCharacters - tekst.length) / 2); i++) {       
        frontStar += '*';
    }
    for (let i = 0; i < (numberOfCharacters - tekst.length - frontStar.length); i++) {
        backStar = backStar + '*';
    }
    document.getElementById('showTrail').textContent = frontStar + tekst + backStar;
})

//16. Użytkownik podaje szer. i wys. tabeli oraz liczbę wierszy. Umieść na stronie tabelę o 3 kolumnach, podanej liczbie wierszy i o podanej wielkości.
document.getElementById('createTable').addEventListener('click', function(e) {
    e.preventDefault();
    const widthTable = parseInt(document.getElementById('width').value, 10);
    const heightTable = parseInt(document.getElementById('height').value, 10);
    const numberOfRows = parseInt(document.getElementById('rows').value, 10);
    const table = document.createElement('TABLE');
    document.getElementById('table').appendChild(table);
    for (let i = 0; i < numberOfRows; i++) {
        let tr = document.createElement('TR');
        table.appendChild(tr);
        for (let j = 0; j < 3; j++) {
            let td = document.createElement('TD');
            tr.appendChild(td);          
        }
    }
    table.setAttribute("width", widthTable + 'px');
    table.setAttribute("height", heightTable + 'px');
})

//17. Wyświetl na stronie listę numerowaną zawierającą kolejnych 100 pierwiastków liczb.
const ol = document.createElement('OL');
document.body.appendChild(ol);
ol.classList.add('result');
for (i = 1; i <= 100; i++) {
    let li = document.createElement('LI');
    ol.appendChild(li);
    li.textContent = Math.sqrt(i)
}

//18. Na stronie umieść formularz umożliwiający wprowadzenie nazwiska, imienia i adresu e-mail. Nasz skrypt po kliknięciu przycisku Oblicz sprawdzi, czy wszystkie pola formularza zostały wypełnione. W przypadku, gdy pole pozostało puste, powinien być wyświetlony komunikat (w oddzielnym oknie dla każdego pola).
document.getElementById('checkData').addEventListener('click', function(e) {
    e.preventDefault();
    let lastName = document.getElementById('lastName');
    const firstName = document.getElementById('firstName');
    const email = document.getElementById('email');
    if (lastName.value.length < 2) {
        lastName.value = 'Podaj nazwisko';
        lastName.classList.add('backlight');
    }
    if (firstName.value.length < 2) {
        firstName.value = 'Podaj imię';
        firstName.classList.add('backlight');
    }
    if (email.value.length < 2) {
        email.value = 'Podaj adres email';
        email.classList.add('backlight');
    }
})

//19. Zdefiniuj funkcję, której działanie będzie polegać na zamianie wpisanej cyfry w polu input na nazwę dnia tygodnia. Jeżeli w inpucie wpiszemy 5 i naciśniemy przycisk "Sprawdź dzień", funkcja ma wyświetlić komunikat "Piątek". Numerację dni tygodnia rozpoczynami od 1 - Poniedziałek, kończymy na 7 - Niedziela. Każda inna wpisana wartość (default) ma spowodować wypisanie komuunikatu "Błędny numer dnia tygodnia"
const zad19 = document.createElement('DIV');
document.body.appendChild(zad19);
zad19.classList.add('rec');
zad19.textContent = 'zad.19';
const enterNumber = document.createElement('INPUT');
zad19.appendChild(enterNumber);
enterNumber.setAttribute('placeholder', 'Podaj cyfrę od 1 do 7');
const checkDay = document.createElement('BUTTON');
zad19.appendChild(checkDay);
checkDay.textContent = 'Sprawdź dzień';
checkDay.addEventListener('click', function(e) {
    e.preventDefault();
    const number = parseInt(enterNumber.value);
    console.log(number);
    switch (number) {
        case 1: {
            enterNumber.value = 'poniedziałek';
            break;
        }
        case 2: {
            enterNumber.value = 'wtorek';
            break;
        }
        case 3: {
            enterNumber.value = 'środa';
            break;
        }
        case 4: {
            enterNumber.value = 'czwartek';
            break;
        }
        case 5: {
            enterNumber.value = 'piątek';
            break;
        }
        case 6: {
            enterNumber.value = 'sobota';
            break;
        }
        case 7: {
            enterNumber.value = 'niedziela';
            break;
        }
        default: {
            enterNumber.value = 'Błędny numer dnia tygodnia';
            break;
        }
    }
})